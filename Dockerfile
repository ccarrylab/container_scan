# Base Dockefile, it will have vulnerabilities
FROM cern/cc7-base

MAINTAINER djuarezg@cern.ch

COPY helloworld.sh /
RUN chmod 700 /helloworld.sh

ENTRYPOINT /helloworld.sh
